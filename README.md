This is my slightly-modified version of Plan 9 from User Space.
Currently, the only changes I've made are in `sam` and `samterm`.

Differences from Main plan9port repo
------------

The only differences (currently) reside in samterm and (to a lesser
extent) sam.

 - sam (and samterm) accepts a new argument, `-i`. Give -i and a
   number, and samterm will indent with that many spaces, instead
   of using tabs. e.g. `sam -i4` will replace tab keystrokes with
   4 spaces. For 4-space indenting with auto-indenting, you would
   run `sam -a -i4`.
 - the delete key now deletes the character immediately following
   dot, instead of the character immediately preceding dot. In
   other words, the delete key used to act like backspace. Now,
   the delete key acts like the delete key.
 - the home key moves dot to the beginning of the current line,
   instead of the beginning of the file. The home key is also
   indent-sensitive, meaning it will move dot to the beginning of
   the non-indent portion of the line, and if it's already there,
   then it will move dot to the true beginning of the line.
 - the end key moves dot to the end of the current line, instead
   of the end of the current file. Out of personal preference,
   the end key is not indent-sensitive.

To-Do List
------------

 - Figure out how to get X11 clipboards working directly with snarf
   (potential hack: just make calls to snarf "magically" run
   `xclip -i <snarf buffer here>`, and make paste "magically"
   run `xclip -o`)

Installation
------------

To install, run ./INSTALL.  It builds mk and then uses mk to
run the rest of the installation.  

For more details, see install(1), at install.txt in this directory
and at https://9fans.github.io/plan9port/man/man1/install.html.

Documentation
-------------

See https://9fans.github.io/plan9port/man/ for more documentation.
(Documentation is also in this tree, but you need to run
a successful install first.  After that, "9 man 1 intro".)

Intro(1) contains a list of man pages that describe new features
or differences from Plan 9.

Helping out
-----------

If you'd like to help out, great!  The TODO file contains a small list.

If you port this code to other architectures, please share your changes
so others can benefit.

Git
---

You can use Git to keep your local copy up-to-date as we make 
changes and fix bugs.  See the git(1) man page here ("9 man git")
for details on using Git.

Status
------

[![Build Status](https://travis-ci.org/9fans/plan9port.svg?branch=master)](https://travis-ci.org/9fans/plan9port)
[![Coverity Scan Build Status](https://scan.coverity.com/projects/plan-9-from-user-space/badge.svg)](https://scan.coverity.com/projects/plan-9-from-user-space)


Contact
-------

* Mailing list: https://groups.google.com/group/plan9port-dev
* Issue tracker: https://github.com/9fans/plan9port/issues
* Submitting changes: https://github.com/9fans/plan9port/pulls

* Russ Cox <rsc@swtch.com>
